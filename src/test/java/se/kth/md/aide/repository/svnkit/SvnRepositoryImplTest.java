package se.kth.md.aide.repository.svnkit;

import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import se.kth.md.aide.repository.api.RepositoryAccessException;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.aide.repository.api.RepositoryFile;
import se.kth.md.aide.repository.api.RevisionAbstractObject;

/**
 * Integration test for {@link SvnRepositoryImpl}.
 * <p>
 * r0 has no files
 * r1 has 1 file (model.reqif,
 * sha256=d1540e68ce40ab111cd4310562b87b944a06411057b53881c9d4edd7b92c8838)
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.13.2
 */
public class SvnRepositoryImplTest {

    public static final String REQIF_EXT = "reqif";
    private final static String SVN_URI = "http://git.md.kth.se:8080/scm/svn/se.kth.md.aide" + "" +
            ".dummy" + ".svn";
    private final static String SVN_USER = "pror.bot";
    private final static String SVN_PASS = "EeChaiqua8yahlahyoli";

    @Test
    public void testSvnRepositoryNoNPE() throws RepositoryAccessException {
        buildSvnRepository();
    }

    @Test
    public void testNrRevisions() throws Exception {
        SvnRepositoryImpl repository = buildSvnRepository();

        List<Revision> revisions = repository.getRevisions();

        assertThat(revisions).hasSize(2);
    }

    @Test
    public void testRevision1HasFile() throws Exception {
        SvnRepositoryImpl repository = buildSvnRepository();
        List<Revision> revisions = repository.getRevisions();
        Revision revision = revisions.get(0);

        List<RevisionAbstractObject> revisionFiles = repository.getRevisionFiles(revision, "");
        assertThat(revisionFiles).hasSize(1);
        assertThat(revisionFiles.get(0).getPath()).endsWith(REQIF_EXT);
    }

    @Test
    public void testRevision1HasModel() throws Exception {
        SvnRepositoryImpl repository = buildSvnRepository();
        Revision revision = repository.getRevisions().get(0);

        assertThat(repository.getRevisionFiles(revision, REQIF_EXT)).hasSize(1);
    }

    @Test
    public void testSizeOfRevision1File() throws Exception {
        SvnRepositoryImpl repository = buildSvnRepository();
        Revision revision = repository.getRevisions().get(0);
        RevisionAbstractObject repositoryFile = repository.getRevisionFiles(revision, "").get(0);

        byte[] revisionObjectByteArray = IOUtils.toByteArray(
                repository.getObjectStream(repositoryFile));

        String sha256Hex = DigestUtils.sha256Hex(revisionObjectByteArray);
        assertThat(revisionObjectByteArray.length).isGreaterThan(0);
        assertThat(sha256Hex).isEqualToIgnoringCase(
                "d1540e68ce40ab111cd4310562b87b944a06411057b53881c9d4edd7b92c8838");
    }

    private SvnRepositoryImpl buildSvnRepository() throws RepositoryAccessException {
        return new SvnRepositoryImpl(SVN_URI, SVN_USER, SVN_PASS);
    }
}
