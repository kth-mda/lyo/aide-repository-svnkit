package se.kth.md.aide.repository.svnkit;

import java.util.function.Predicate;
import se.kth.md.aide.repository.api.PathFilter;
import se.kth.md.aide.repository.api.RevisionAbstractObject;

/**
 * Created on 12.03.17
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public class SvnPathFilterImpl implements PathFilter {
    private final String extension;

    public SvnPathFilterImpl(final String extension) {
        this.extension = extension;
    }

    @Override
    public boolean isValid(final String path, final boolean isDirectory) {
        return path.endsWith(extension) && !isDirectory;
    }

    Predicate<RevisionAbstractObject> getInversePredicateFilter() {
        return e -> !e.getPath().endsWith(extension);
    }
}
