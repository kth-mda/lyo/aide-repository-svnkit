package se.kth.md.aide.repository.svnkit;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import se.kth.md.aide.repository.api.Filter;
import se.kth.md.aide.repository.api.FilterAbstractFactory;
import se.kth.md.aide.repository.api.Repository;
import se.kth.md.aide.repository.api.RepositoryAccessException;
import se.kth.md.aide.repository.api.RepositoryFile;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.aide.repository.api.RevisionAbstractObject;

/**
 * SvnRepositoryImpl is .
 *
 * @author Mustafa Elbehery
 * @since 0.12.0
 */
public class SvnRepositoryImpl implements Repository {

    private final static String DEFAULT_PATH_FILTER = "";
    private final Object lock = new Object();
    private SVNRepository repository;
    private SvnFilterAbstractFactoryImpl abstractFactory;

    public SvnRepositoryImpl(URI svnRepoURI, String username, String password)
            throws RepositoryAccessException {
        this(svnRepoURI.toString(), username, password);
    }

    public SvnRepositoryImpl(String url, String username, String password)
            throws RepositoryAccessException {

        try {
            this.repository = createSVNRepositoryDriver(url, username, password);
        } catch (SVNException ex) {
            throw new RepositoryAccessException("Authentication Parameters are not correct", ex);
        }
    }

    @Override
    public List<Revision> getRevisions() {

        Collection<SVNLogEntry> logEntries = null;
        try {
            logEntries = this.repository.log(new String[]{""}, null, -1, 0, false, true);
        } catch (SVNException ex) {
            return Collections.emptyList();
        }

        // Result List.
        List<Revision> revisionList = new ArrayList<>(logEntries.size());

        Iterator<SVNLogEntry> logIterator = logEntries.iterator();
        while (logIterator.hasNext()) {
            SVNLogEntry currentEntry = logIterator.next();
            revisionList.add(new Revision(String.valueOf(currentEntry.getRevision()),
                    currentEntry.getDate().toInstant().atZone(ZoneId.systemDefault())));
        }

        return revisionList;
    }

    @Override
    public List<RevisionAbstractObject> getRevisionFiles(Revision rev, String pathFilter)
            throws RepositoryAccessException {
        return getRevisionObjects(rev, getFilterFactory().getPathFilter(pathFilter));
    }

    @Override
    public List<RevisionAbstractObject> getRevisionObjects(final Revision rev, final Filter filter)
            throws RepositoryAccessException {
        List<RevisionAbstractObject> resultList = new ArrayList<>();
        try {
            SVNNodeKind nodeKind = this.repository.checkPath(DEFAULT_PATH_FILTER,
                    Long.valueOf(rev.getId()));
            if (nodeKind == SVNNodeKind.FILE || nodeKind == SVNNodeKind.NONE || nodeKind ==
                    SVNNodeKind.UNKNOWN) {
                return resultList;
            } else if (nodeKind == SVNNodeKind.DIR) {
                getDirectoryFiles(rev, DEFAULT_PATH_FILTER, resultList);
            }
        } catch (SVNException e) {
            throw new RepositoryAccessException(e);
        }

        // workaround, filtering irrelevant files out.
        // TODO: 12.03.17 ping Mustafa about some email he sent to the mailing list
        // http://subversion.1072662.n5.nabble
        // .com/Get-All-Files-on-a-Specific-Revision-td197895.html
        // haha don't think we should expect a reply to that :)
        if (filter instanceof SvnPathFilterImpl) {
            resultList.removeIf(((SvnPathFilterImpl) filter).getInversePredicateFilter());
        }

        return resultList;
    }

    @Override
    public InputStream getObjectStream(final RevisionAbstractObject fileObject) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        SVNProperties fileProperties = new SVNProperties();

        try {
            this.repository.getFile(fileObject.getPath(),
                    Long.valueOf(fileObject.getRevision().getId()), fileProperties,
                    byteArrayOutputStream);
        } catch (SVNException e) {

            throw new IOException(
                    "Error reading the file at " + fileObject.getPath() + " in " + "revision " +
                            fileObject

                            .getRevision().getId(), e);
        }

        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }

    @Override
    public FilterAbstractFactory getFilterFactory() {
        if (abstractFactory == null) {
            synchronized (lock) {
                // TODO: 12.03.17 maybe need to make it volatile
                if (abstractFactory == null) {
                    abstractFactory = new SvnFilterAbstractFactoryImpl();
                }
            }
        }
        return abstractFactory;
    }

    private SVNRepository createSVNRepositoryDriver(String url, String username, String password)
            throws SVNException {

        DAVRepositoryFactory.setup();
        SVNRepository svnRepository = DAVRepositoryFactory.create(SVNURL.parseURIEncoded(url));
        ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(
                username, password);
        svnRepository.setAuthenticationManager(authManager);

        return svnRepository;
    }

    private void getDirectoryFiles(Revision rev, String path,
            List<RevisionAbstractObject> resultList) throws SVNException {

        Collection<SVNDirEntry> entries = this.repository.getDir(path, Long.valueOf(rev.getId()),
                null, SVNDirEntry.DIRENT_ALL, (Collection) null);

        for (final SVNDirEntry entry : entries) {
            resultList.add(new RepositoryFile(rev, entry.getRelativePath()));

            if (entry.getKind() == SVNNodeKind.DIR) {
                getDirectoryFiles(rev,
                        (path.equals("")) ? entry.getName() : path + "/" + entry.getName(),
                        resultList);
            }
        }
    }

}
